const elements = document.querySelectorAll('.edit--btn');

elements.forEach(elem => {
    elem.addEventListener('click', () => {
        let cmd = elem.dataset['command'];
        if (cmd === 'createlink' || cmd === 'insertImage') {
            let url = prompt("Enter the link here: ", "http:\/\/");
            document.execCommand(cmd, false, url);
        } else {
            document.execCommand(cmd, false, null);
        }
    })
})